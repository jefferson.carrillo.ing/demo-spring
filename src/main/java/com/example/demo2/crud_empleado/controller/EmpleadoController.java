 
package com.example.demo2.crud_empleado.controller;

import java.util.List;

import com.example.demo2.crud_empleado.entity.Empleado;
import com.example.demo2.crud_empleado.service.EmpleadoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmpleadoController {
    @Autowired
    private EmpleadoService service;

    @GetMapping("/")
    public String VistaListaEmpleado(Model modelo) {
        List<Empleado> ListaEmpleados = service.MostarTodo();
        modelo.addAttribute("ListaEmpleados", ListaEmpleados);
        return "prueba_crud_empleado/index";
    }

    @GetMapping("/new")
    public String VistaNuevoEmpleado(Model model) {
        model.addAttribute("Empleados", new Empleado());
        model.addAttribute("titulo", "Crear nuevo empleado");
        return "prueba_crud_empleado/new";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String GuardarEmpleado(@ModelAttribute("Empleados") Empleado empleado) {
        service.Guardar(empleado);
        return "redirect:/";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView VistaEditarEmpleado(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("prueba_crud_empleado/new");
        Empleado empleado = service.MostrarId(id);
        mav.addObject("Empleados", empleado);
        mav.addObject("titulo", "Editar empleado");
        return mav;
    }

    @RequestMapping("/delete/{id}")
    public String EmliminarEmpleado(@PathVariable(name = "id") int id) {
        service.Eliminar(id);
        return "redirect:/";
    }

}
