package com.example.demo2.crud_empleado.repository;
import com.example.demo2.crud_empleado.entity.Empleado;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Integer>{


} 
    

