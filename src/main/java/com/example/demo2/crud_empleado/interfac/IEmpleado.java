package com.example.demo2.crud_empleado.interfac;

import java.util.List;

import com.example.demo2.crud_empleado.entity.Empleado;

public interface IEmpleado {

    public List<Empleado> MostarTodo();

    public void Guardar(Empleado empleado);

    public Empleado MostrarId(Integer id);

    public void  Eliminar(Integer id);

}
