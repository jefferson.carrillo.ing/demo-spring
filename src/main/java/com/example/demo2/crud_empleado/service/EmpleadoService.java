package com.example.demo2.crud_empleado.service;

import java.util.List;

import com.example.demo2.crud_empleado.entity.Empleado;
import com.example.demo2.crud_empleado.interfac.IEmpleado;
import com.example.demo2.crud_empleado.repository.EmpleadoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpleadoService implements IEmpleado {

    @Autowired
    private EmpleadoRepository repo;

    @Override
    public List<Empleado> MostarTodo() {
        // TODO Auto-generated method stub
        return repo.findAll();
    }

    @Override
    public void Guardar(Empleado std) {
        // TODO Auto-generated method stub
        repo.save(std);

    }

    @Override
    public Empleado MostrarId(Integer id) {
        // TODO Auto-generated method stub
        return repo.findById(id).get();
    }

    @Override
    public void Eliminar(Integer id) {
        // TODO Auto-generated method stub
        repo.deleteById(id);
    }

}
