package com.example.demo2.crud_persona_api.interfac;

import java.util.List;

import com.example.demo2.crud_persona_api.entity.Persona;

public interface IPersona {

    public List<Persona> MostarTodo();

    public Persona Guardar(Persona persona);

    public Persona Buscar(Integer  id);

    public Persona Eliminar(Integer id);

}
