
package com.example.demo2.crud_persona_api.controller;

import java.util.List;

import com.example.demo2.crud_persona_api.entity.Persona;
import com.example.demo2.crud_persona_api.model.peticion;
import com.example.demo2.crud_persona_api.service.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE, RequestMethod.OPTIONS, RequestMethod.PUT})
@RestController
@RequestMapping("/api/persona")
public class PersonaController {

    @Autowired
    private PersonaService service;

    @RequestMapping(value = "/mostrar", method = RequestMethod.GET)
    public peticion MostrarPersona() {
        peticion res = new peticion();
        try {
            List<Persona> ListaPersona = service.MostarTodo();
            res.setData(ListaPersona);
            res.setMensaje("Registros encontrados: " + ListaPersona.size());
        } catch (Exception e) {
            res = new peticion(403, e.getMessage(), e);
        }

        return res;
    }

    @RequestMapping(value = "/buscar", method = RequestMethod.POST)
    public peticion BuscarPersona(@RequestBody Persona persona) {
        peticion res = new peticion();
        try {
            persona = service.Buscar(persona.getId());
            res.setData(persona);
            res.setMensaje(persona.getNombre() + " " + persona.getApellido() + " encontrada.");
        } catch (Exception e) {
            res = new peticion(403, e.getMessage(), e);
        }

        return res;
    }

    @RequestMapping(value = "/guardar", method = RequestMethod.POST)
    public peticion GuardarPersona(@RequestBody Persona persona) {
        peticion res = new peticion();
        try {
            Persona nueva_persona = service.Guardar(persona);
            res.setData(nueva_persona);
            res.setMensaje(nueva_persona.getNombre() + " " + nueva_persona.getApellido() + " registrado.");
        } catch (Exception e) {
            res = new peticion(403, e.getMessage(), e);
        }

        return res;
    }

    @RequestMapping(value = "/eliminar/", method = RequestMethod.DELETE)
    public peticion EliminarPersona(Integer id) {
        peticion res = new peticion();
        try {
            Persona persona = service.Buscar(id);
            if (persona == null) {
                res.setStatus(403);
                res.setMensaje("No existe persona " + id);
            } else {
                service.Eliminar(id);
                res.setData(persona);
                res.setMensaje(persona.getNombre() + " " + persona.getApellido() + " eliminado.");
            }

        } catch (Exception e) {
            res = new peticion(403, e.getMessage(), e);
        }

        return res;
    }

    /*
     * @RequestMapping(value = "/persona/guardar", method = RequestMethod.POST)
     * public String Guardarpersona(@ModelAttribute("Persona") Persona persona) {
     * service.Guardar(persona); return "redirect:/"; }
     * 
     * @RequestMapping(value = "persona/edit/{id}", method = RequestMethod.POST)
     * public ModelAndView VistaEditarpersona(@PathVariable(name = "id") int id) {
     * ModelAndView mav = new ModelAndView("prueba_crud_persona_api/new"); Persona
     * persona = service.MostrarId(id); mav.addObject("Persona", persona);
     * mav.addObject("titulo", "Editar persona"); return mav; }
     * 
     * @RequestMapping("/delete/{id}") public String
     * Emliminarpersona(@PathVariable(name = "id") int id) { service.Eliminar(id);
     * return "redirect:/"; }
     */

}
