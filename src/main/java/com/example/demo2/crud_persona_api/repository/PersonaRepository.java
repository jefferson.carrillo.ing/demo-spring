package com.example.demo2.crud_persona_api.repository;
import com.example.demo2.crud_persona_api.entity.Persona;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Integer>{


} 
    

