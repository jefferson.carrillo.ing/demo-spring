package com.example.demo2.crud_persona_api.service;

import java.util.List;

import com.example.demo2.crud_persona_api.entity.Persona;
import com.example.demo2.crud_persona_api.interfac.IPersona;
import com.example.demo2.crud_persona_api.repository.PersonaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaService implements IPersona {

    @Autowired
    private PersonaRepository repo;

    @Override
    public List<Persona> MostarTodo() {
        return repo.findAll();
    }

    @Override
    public Persona Guardar(Persona persona) {
        return repo.save(persona);

    }

    @Override
    public Persona Buscar(Integer id) {
        return repo.findById(id).get();
    }

    @Override
    public Persona Eliminar(Integer id) {
        Persona persona = repo.findById(id).get();
        if (persona != null) {
            repo.deleteById(persona.getId());
        }

        return persona;
    }

}
