package com.example.demo2.crud_persona_api.model;

public class peticion {

    private int status = 200;
    private String mensaje;
    private Object data;

    public peticion() {
        super();
    }

    public peticion(int status, String mensaje, Object data) {
        this.status = status;
        this.mensaje = mensaje;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
