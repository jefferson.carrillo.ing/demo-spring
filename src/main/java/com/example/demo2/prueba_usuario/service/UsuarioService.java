package com.example.demo2.prueba_usuario.service;

import java.util.ArrayList;
import java.util.List;

import com.example.demo2.prueba_usuario.model.Usuario;

import org.springframework.stereotype.Service; 

@Service
public class UsuarioService {
 
    public List<Usuario> ListarUsuario() {
        List<Usuario> lista_usuario = new ArrayList<>();
        lista_usuario.add(new Usuario(3, "333333333333333", "ALEXY", "Andrade"));
        lista_usuario.add(new Usuario(4, "444444444444444", "ALEXY", "Andrade"));
        lista_usuario.add(new Usuario(5, "5555555555555", "ALEXY", "Andrade"));
        lista_usuario.add(new Usuario(6, "66666666666666", "ALEXY", "Andrade"));
        lista_usuario.add(new Usuario(7, "7777777777777777", "ALEXY", "Andrade"));
        lista_usuario.add(new Usuario(8, "888888888888888", "ALEXY", "Andrade"));
        lista_usuario.add(new Usuario(9, "9999999999999", "ALEXY", "Andrade"));

        Usuario persona1 = new Usuario();
        persona1.setId(1);
        persona1.setDni("11111111111");
        persona1.setApellido("carrillo");
        persona1.setNombre("jefferson");

        String dni = persona1.getDni();
        System.out.println(dni);

        lista_usuario.add(persona1);

        Usuario persona2 = new Usuario(2, "12222222222222", "ALEXY", "Andrade");
        lista_usuario.add(persona2);
        return lista_usuario;
    }

}
