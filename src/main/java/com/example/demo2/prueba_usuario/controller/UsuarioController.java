package com.example.demo2.prueba_usuario.controller;
 
import java.util.List;

import com.example.demo2.prueba_usuario.model.Usuario;
import com.example.demo2.prueba_usuario.service.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @RequestMapping("/usuario/listar")
    public List<Usuario> usuario() {
        return service.ListarUsuario();
    }

}
